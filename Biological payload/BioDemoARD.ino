/* 
    OPEN COSMOS 
    beeKit IoT demo (Arduino code)
    
    This sketch mimics the performance of a Biological payload:
    - Makes the calculations to control the temperature of the sample and send the required
    control request via I2C.
    - Dislays the status of the sample temperature using a RGB LED.

    It demonstrates the use of I2C port between beeKit and arduino.
*/

#include<Wire.h>   

int TempTarget = 32;
int TempTolerance = 2;

double Kp = 3;            
double Ki = 0.000001;
double Kd = 0.5;

float OffBKVoltage = 0.0;     //Voltage levels of beeKit HV power rail
float MinBKVoltage = 6.0;
float MaxBKVoltage = 12.0;

float CalSpan = 1.0;          //Calivration parameters of the temperature Sensor
float CalOffset = -7.0;

int LoopTime = 1000;

int TempSenPin = 18;
int LightSenPin = 19;
int RedLED = 10;
int GreenLED = 16;
int BlueLED = 14;

const int messageSize = 6;
byte i2cMessage[messageSize];

unsigned long currentTime, previousTime;
double elapsedTime;
double lastError;
double error, intError, derError;
double input, output, rawOutput;

float tempRaw;
float tempMiliV;
float tempCelsius;
float lightRaw;
float lightInt;

void setup() {
  Serial.begin(115200);               //Initialize the Serial port - USB
  Serial1.begin(115200);              //Initialize the Serial port - Pins
  Wire.begin(8);                      // Initiate the Wire library (I2C)with address 8
  Wire.onRequest(requestEvent);       // register event
  
  delay(300);
  Serial.println("setting up...");    //Print message throught the Serial port

  pinMode(TempSenPin,INPUT);       
  pinMode(LightSenPin, INPUT);                    
  pinMode(RedLED, OUTPUT);                            
  pinMode(GreenLED, OUTPUT);            
  pinMode(BlueLED, OUTPUT);                         

  delay(100);

  digitalWrite(RedLED, LOW);
  digitalWrite(GreenLED, LOW);
  digitalWrite(BlueLED, LOW);
}

void loop() {

  tempRaw = analogRead(TempSenPin);                         //Reading the value from sensor
  tempCelsius=(tempRaw*500/1023)* CalSpan + CalOffset;      //Sensor normaization and calibration
  i2cMessage[0] = tempCelsius;                              //write the temperature integer into the message
  i2cMessage[1] = (tempCelsius - (int)tempCelsius) * 100;   //write the temperature decimals into the message

//  lightRaw = analogRead(LightSenPin);
//  lightInt = (lightRaw - 500)*0.2;

  Serial.print("Temp = ");
  Serial.print(tempCelsius);
  Serial.print("\t");
  Serial.print("TempErr = ");
  Serial.print(tempCelsius - TempTarget);
  Serial.print("\t");
//  Serial.print("Light = ");
//  Serial.println(lightInt);

  rawOutput = tempPID(tempCelsius);       //Calculate the controller output
  
  if (rawOutput < MinBKVoltage){          //Normalize the output beetween Min and Max voltage
    output = OffBKVoltage;
  }else if (rawOutput > MaxBKVoltage){
    output = MaxBKVoltage;
  }else{
    output = rawOutput;
  }
  i2cMessage[2] = output;                 //write the normalized output into the message
        
  Serial.print("rawOutput = ");
  Serial.print(rawOutput);
  Serial.print("\t");
  Serial.print("output = ");
  Serial.print(output);
  Serial.println();
  
  if (tempCelsius < (TempTarget-(2*TempTolerance))){      //Manage the LEDs colours
    digitalWrite(RedLED, LOW);
    digitalWrite(GreenLED, LOW);
    digitalWrite(BlueLED, HIGH);
  }else if ((tempCelsius > (TempTarget-(2*TempTolerance))) && (tempCelsius < (TempTarget-TempTolerance))){
    digitalWrite(RedLED, LOW);
    digitalWrite(GreenLED, HIGH);
    digitalWrite(BlueLED, HIGH);
  }else if ((tempCelsius > (TempTarget-TempTolerance)) && (tempCelsius < (TempTarget+TempTolerance))) {
    digitalWrite(RedLED, LOW);
    digitalWrite(GreenLED, HIGH);
    digitalWrite(BlueLED, LOW);
  }else if ((tempCelsius > (TempTarget+TempTolerance)) && (tempCelsius < (TempTarget+(2*TempTolerance)))){
    digitalWrite(RedLED, HIGH);
    digitalWrite(GreenLED, HIGH);
    digitalWrite(BlueLED, LOW);
  }else if (tempCelsius > (TempTarget+(2*TempTolerance))){
    digitalWrite(RedLED, HIGH);
    digitalWrite(GreenLED, LOW);
    digitalWrite(BlueLED, LOW);
  }else{
    digitalWrite(RedLED, LOW);
    digitalWrite(GreenLED, LOW);
    digitalWrite(BlueLED, LOW);
  }


   i2cMessage[3] = digitalRead(RedLED);        //write the LEDs state into the message
   i2cMessage[4] = digitalRead(GreenLED);
   i2cMessage[5] = digitalRead(BlueLED);
   
  delay(LoopTime);
}

void requestEvent() {
  
  Wire.write(i2cMessage,messageSize);
  Serial.println("i2c message sent");
}

double tempPID(double input){  
        
        currentTime = millis();
        elapsedTime = (double)(currentTime - previousTime);
        
        error = TempTarget - input;                     // determine error
        intError += error * elapsedTime;                // compute integral
        derError = (error - lastError)/elapsedTime;     // compute derivative

        output = Kp*error + Ki*intError + Kd*derError;  //PID output               
        
        lastError = error;                              //remember current error
        previousTime = currentTime;                     //remember current time
 
        return output;                                  //have function return the PID output
}
