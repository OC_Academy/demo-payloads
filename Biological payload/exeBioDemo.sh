#!/bin/bash

cd /home/OC_demos
./BioDemoBK.py &

cd /oc/bin/api
while [ "$(expr index "$(./zmq_client registers user_name g payload_off_pending)" "1")" != "21" ]
do
	sleep 1
done

kill $(jobs -p)
cd /home/OC_demos

