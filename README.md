Demo payloads is a repository that supports the development of space technologies and applications using a set of tools and platforms developed by Open Cosmos.
The integration of non-space rated components is done in a similar way as real small satellite instruments and subsystems, serving as a reference guide among a broader audience that might not have space background or experience.

This project gathers all the open source resources produced by the Open Cosmos Academy initiative including step-by-step tutorials to develop all kinds of demo payloads that can later be integrated into real satellite qualification platforms, both: software (beeApp) and hardware (beeKit). More information can be found in the wiki section of this project: https://gitlab.com/OC_Academy/demo-payloads/wikis

If you would like to contribute, please send an email with your gitlab username to: support@open-cosmos.com