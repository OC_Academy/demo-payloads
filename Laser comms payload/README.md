# Laser communication payload

This payload was delivered as part of a larger scale project aimed to demonstrate a proof-of-concept optical communication link between two identical payloads installed into two *beeKit* platforms.

![Laser_comms_demo_payload2](images/assembly.jpg)

## Electronics

Electronic circuit can be assembled on one standard size breadboard.

![ESP32-1](images/schematic.png)

### List of components required to assemble one unit:
* 1x ESP32 Adafruit HUZZAH Feather Board
* 1x LT1363 high speed op-amp
* 1x MCP4131-103 10k digital potentiometer
* 1x MCP41100 100k digital potentiometer
* 1x Standard 3mW 650nm laser diode
* 1x FDS100 photodiode
* 3x 2N3904 N-Channel transistors
* 1x 2N7000 N-Channel MOSFET
* 1x Active Buzzer
* 3x 10uF electrolytic capacitors
* 4x 0.1uF ceramic capacitors
* 2x LEDs
* 4x 4.7k; 1x 3k; 3x 1k; 2x 100 resistors
* 10k & 100 potentiometers
* 1x 3V3 rated zener diode
* 2x standard diodes

## beeKit interface

Electronics with *beeKit* are interfaced using Serial protocol at baud 460800,8N1 configuration using two wires (Tx & Rx). Electronics module is powered using +5V supplied by beeKit's power cable.

## Software

Software for ESP-32 micro-controller can be found [here](https://github.com/elementweb/ESP32-OCP). Software can be flashed into ESP32 using Visual Studio Code with PlatformIO extension.

The following diagram represents the software model that is used in the design.

![system-model-reduced](images/model.png)

### beeKit Python scripts

Python/Shell code required for demonstration can be found [here](https://gitlab.com/OC_Academy/demo-payloads/tree/master/Laser%20comms%20payload).

## Final assembly

Figure below shows the internal assembly of payload module with key components outlined: (a) data cable, (b) power cable, (c) microSD card module, (d) photodiode – receiver, (e) laser - transmitter, (f) operational amplifier, (g) – ESP-32 module – MCU, (h) indicator LEDs.

![topview](images/topview.png)

## Testing & verification

1. With two electronic modules assembled, connect 4 wires (UART_Rx, UART_Tx, +5V and GND) to each beeKit
2. Using USB cable, flash ESP32 firmware using Visual Studio Code and PlatformIO
3. Upload Python/Shell code (3 files) into both *beeKit*s using File Manager within HIL module to `OC_Demos` folder
4. Run `LaserCommsSendMessage.sh` one the first beeKit, two short beeps indicate that boot-up has been successful and laser will start blinking
5. Run `LaserCommsReceiveMessage.sh` on second beeKit, two short beeps indicate the successful boot-up of payload module
6. Point laser of `payload #1` to a photodiode of `payload #2` and vice versa
7. When laser beams are aligned successfully, a series of short burst will indicate active data exchange between two payload modules
8. On successful data transmission, `payload #2` will contain `received.txt` file in the same `OC_Demos` directory with multiple lines of the following message: `This is a test message to be sent over optical link.`

With current configuration, data transfer speeds of up to 160kbps could be achieved.

### Node.js validation software

Evaluation software for testing assembled model can be found [here](https://github.com/elementweb/ESP32-OCP-EVAL). Standard UART cable is required to interface payload unit.

The following commands are available:
*  `message` & `listen` commands - send/receive short text messages between two computers – script encodes and feeds message into payload unit over UART interface, as well as it decodes incoming messages and prints them in real-time.
* `send <filename>` - Encodes files in base64 and sends them as a stream of bytes into the payload unit. Based on file size, this will be split into a number of packets demonstrating successful data transmission with multiple packet sequences. Note that all base64 encoded data inflates by around 30% of its original capacity.
* `receive` - reverses the above process, decodes base64 encrypted files and saves them as files on the remote computer. The script demonstrates a successful file transmission between two payload units and also that any type of file can be sent over optical link. Files with the following have been tested: .zip, .csv, .pdf, .png, .jpg, .txt, .mp3.
