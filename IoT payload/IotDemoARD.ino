/* 
    OPEN COSMOS 
    beeKit IoT demo (Arduino code)
    
    This sketch mimics the gathering of information from external nodes as it would be done
    in an IoT aplication:
    - Reads temperature and pressure data on the flight unit and receives information from 
    external CanSat nodes and sends it to I2C port.

    It demonstrates the use of I2C port between beeKit and arduino. 
*/

//Include the required libraries
#include <qbcan.h>
#include <Wire.h>
#include <SPI.h>

//Radio Parameters
#define NODEID        1    //unique for each node on same network
#define NETWORKID     100  //the same on all nodes that talk to each other
#define ENCRYPTKEY    "sampleEncryptKey" //exactly the same 16 characters/bytes on all nodes!
#define I2CBUFFSIZE   32   //Wire library buffer limit

char BeginTx = 0x01;
char EndTx = 0x04;
char BeginBlock = 0x02;
char EndBlock = 0x03;

double bmpTemp, bmpPres;
String i2cStrMsg, stringMessage, tempStr, presStr;
uint32_t packetCountNode = 0;
uint32_t packetCountHub = 0;
uint32_t packetCount = 0;

//Radio object
RFM69 radio;
bool promiscuousMode = false; //set to 'true' to sniff all packets on the same network

//Pressure sensor object
BMP180 bmp;

void setup()
{
  Serial.begin(115200);               //Initialize the Serial port - USB
  Serial.println("REBOOT");

  Wire.begin(9);                      // Initiate the Wire library (I2C)with address 8
  Wire.onRequest(requestEvent);       // register event

 // Initialize pressure sensor.
  if (bmp.begin())
    Serial.println("BMP180 init success");
  else
  {
    //In case of error let user know of the problem
    Serial.println("BMP180 init fail (disconnected?)\n\n");
    while(1); // Pause forever.
  }


  //Delay to give time to the radio to power up
  delay(1000);

  //Initialize radio
  radio.initialize(FREQUENCY,NODEID,NETWORKID);
  radio.setHighPower(); //Use the high power capabilities of the RFM69HW
  radio.encrypt(ENCRYPTKEY);
  radio.promiscuous(promiscuousMode);
  Serial.println("Listening at 433 Mhz");

}

void loop()
{ 
  
  bmp.getData(bmpTemp,bmpPres);         // Get a new sensor readings:  
  tempStr = String(bmpTemp);
  presStr = String(bmpPres);
  stringMessage = String(stringMessage + BeginBlock + "[Payload " + NODEID + "][" + ++packetCountHub+ "]");
  stringMessage = String(stringMessage + "T: " + tempStr + " C, P: " + presStr + "mb.");
  stringMessage = String(stringMessage + EndBlock);

  if (radio.receiveDone()){             // Get nodes data 
    Serial.println("Data received");
    stringMessage = String(stringMessage + BeginBlock + "[Node " + radio.SENDERID + "][" + ++packetCountNode + "][" + radio.RSSI + "]");

    for (byte i = 0; i < radio.DATALEN; i++){
      if (radio.DATA[i] != 0){
        stringMessage = String(stringMessage + (char)radio.DATA[i]);
      }
    }
    stringMessage = String(stringMessage + EndBlock);
  }

  delay(1000);
}

void requestEvent() {

  int msgLen = stringMessage.length()+1;                      // obtain length of string w/ terminator

  if (msgLen > I2CBUFFSIZE){                
    
    i2cStrMsg = stringMessage.substring(0,I2CBUFFSIZE-2);     // cut the string into sendable pieces
    stringMessage = stringMessage.substring(I2CBUFFSIZE-2);
    
    char i2cCharMsg[I2CBUFFSIZE];                             // create character array
    i2cStrMsg.toCharArray(i2cCharMsg, I2CBUFFSIZE);           // copy string to character array
    Wire.write(i2cCharMsg);
    Serial.println(i2cCharMsg);  
  
  }else if (msgLen != 0){

    i2cStrMsg = stringMessage;
    stringMessage = (String)"";
    
    char i2cCharMsg[msgLen];                                  // create character array
    i2cStrMsg.toCharArray(i2cCharMsg, msgLen);                // copy string to character array
    Wire.write(i2cCharMsg);
    Serial.println(i2cCharMsg);
    
    Wire.write(EndTx); 
     
  }else{
    Wire.write(EndTx);
  }
  
  Serial.println("i2c message sent");
}
