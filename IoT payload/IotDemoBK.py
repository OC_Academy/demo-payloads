#!/usr/bin/python

from smbus2 import SMBus
import time
import datetime
import subprocess 

ZmqAddress = "/oc/bin/api/zmq_client"
CmdZmqHvOn = "/oc/bin/api/zmq_client power user p hv 1"
CmdZmqHvOff = "/oc/bin/api/zmq_client power user p hv 0"
CmdZmq5vOn = "/oc/bin/api/zmq_client power user p 5v0 1"
CmdZmq5vOff = "/oc/bin/api/zmq_client power user p 5v0 0"
CmdZmqHvSet = "/oc/bin/api/zmq_client power user v %d"
CmdZmqLedSet = "/oc/bin/api/zmq_client leds user s %d %d"
CmdZmqBattLowGet = "/oc/bin/api/zmq_client registers user g battery_low"
CmdZmqBattLowSet = "/oc/bin/api/zmq_client registers user s battery_low %d"

LoopTime = 0.5
runTime = 10
addr = 0x9
messageSize = 32

photoTaken = False
battLow = False
i2cAvailable = False

beginTx = '\x01'
endTx = '\x04'
beginBlock = '\x02'
endBlock = '\x03'

subprocess.Popen(CmdZmqLedSet % (0,0), shell=True)
subprocess.Popen(CmdZmqLedSet % (1,0), shell=True)
subprocess.Popen(CmdZmqLedSet % (2,0), shell=True)
subprocess.Popen(CmdZmqLedSet % (3,0), shell=True)
subprocess.Popen(CmdZmqLedSet % (4,0), shell=True)
subprocess.Popen(CmdZmqBattLowSet % 0, shell=True)	


startTime = time.time()
dateTime = datetime.datetime.now()
dateTimeStr = dateTime.strftime("%Y-%m-%d_%H:%M:%S")
logFileName = "iotLog" + dateTimeStr + ".txt"

subprocess.Popen(CmdZmqLedSet % (0,1), shell=True)
bus = SMBus(1)

subprocess.Popen(CmdZmq5vOn, shell=True)
#subprocess.Popen(CmdZmqHvSet % 6000, shell=True)
#subprocess.Popen(CmdZmqHvOn, shell=True)	

time.sleep(5)
subprocess.Popen(CmdZmqLedSet % (1,1), shell=True)


while (not battLow):

	i2cAvailable = True
	msgList = ""
		
	while i2cAvailable:
		inputChar = bus.read_i2c_block_data(addr,0,messageSize)
		for i in inputChar:
			c = chr(i)
			if (c == endTx):
				i2cAvailable = False
				break
			elif (c == beginBlock or c == beginTx):
				msgList += ''	
			elif (c == endBlock):
				msgList += '\n'
			elif (c != chr(255)):
				msgList += c

	print(msgList)

	now = datetime.datetime.now() 
	nowStr = now.strftime("%H:%M:%S")

	with open(logFileName,'a') as file:
		file.write("--")
		file.write(nowStr)
		file.write("--")
		file.write('\n')
		file.write(msgList)
		file.close()
		
	timeCount = time.time() - startTime 
	battStatus = subprocess.run(CmdZmqBattLowGet, shell=True, stdout=subprocess.PIPE)	
	if ('1' in battStatus.stdout.decode("utf-8")):
		battLow = True
	
	subprocess.Popen(CmdZmqLedSet % (4,1), shell=True)
	time.sleep(LoopTime)
	subprocess.Popen(CmdZmqLedSet % (4,0), shell=True)


print("closing...")
time.sleep(5)

subprocess.Popen(CmdZmqLedSet % (0,0), shell=True)
subprocess.Popen(CmdZmqLedSet % (1,0), shell=True)
subprocess.Popen(CmdZmqLedSet % (2,0), shell=True)
subprocess.Popen(CmdZmqLedSet % (3,0), shell=True)
subprocess.Popen(CmdZmqLedSet % (4,0), shell=True)

subprocess.Popen(CmdZmq5vOff, shell=True)
